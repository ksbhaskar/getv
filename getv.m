;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;								;
; Copyright (c) 2018-2024 K.S. Bhaskar				;
; All rights reserved.                                          ;
;								;
;	This source code contains the intellectual property	;
;	of its copyright holder(s), and is made available	;
;	under a license.  If you do not know the terms of	;
;	the license, please stop and do not read further.	;
;								;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
getv
	; Interact with voter data, usually available from states.
	; Usage: yottadb -run $text(+0) [state] op [options] where op and options are:
	; analyze - Generate suggestions for pruning and seeding from curated list
	;   --addtrig <ratio> - a value >0 and <1; if the ratio of Y tags to Y+N tags for a name
	;     not on the list exceeds this number, it is recommended for seeding; defaults to .6
	;   --curate <field> - field (column) with curating, Y, N or blank
	;   --deltrig <ratio> - a value >0 and <1; if the ratio of Y tags to Y+N tags for a name
	;     on the list is less than this number, it is recommended for pruning; defaults to .8
	;   --fields <fields> - fields (columns) with voter names, e.g., "5,6,7"
	;   --header - if first line is column headers
	;   --names <tag> - where <tag> is a list of names previously imported by importnames; if
	;     <tag> does not have a version number, it uses the latest version of that list
	;   --sep <sep> - field separator for the input file, defaults to tab
	; clean - Clean voter records where first, middle, or last name has bad data
	; expand - on stdout, produce expanded list of names with other associated names
	;   --detail - list count of matches & non-matches with names on original list
	;   --minmatch - number of minimum matches (to eliminate typos and singleton names), defaults to 2
	;   --strength - fraction (0<strength<1) of associated names already in list, defaults to .8
	;   --cycles - number of iterations of expansion, defaults to 1
	; fields - Read definitions of fields of voter file from stdin or file <state>Fields.txt
	;   CAUTION: very state specific
	; help - Output options to use this program
	; importnames <tag> where <tag> is a label for these names, e.g., desis_20200417
	;   - Each line of stdin is assumed to be a name to be loaded into ^names(<tag>)
	; importvoters import voter file from stdin, one voter to a line
	; names - processing associated with names
	;   --analyze <file> - read a list of names from <file> and output summary of associated names
	;   --find <pattern> - across all states find voter names that match the M pattern
	;   --summarize - equivalent to analyze of names of all voters
	; printvoters - on stdout, output list of voter records as specified
	;   --bornafter <date> - date of birth is after date (default is 12/31/1840)
	;   --bornbefore <date> - date of birth is before date (default is TODAY)
	;   --county <countylist> - voters for the selected counties
	;     <countylist> is comma separated list of counties, all upper case ("*" to select all)
	;     defaults to * if not specified
	;   --exclude <parties> - exclude voters with these party affiliations
	;     <parties> is comma separated list of parties (default --exclude R)
	;     only one of --select and --exclude are meaningful
	;   --gender - select specified gender
	;   --names <tag> - where <tag> is a list of names previously imported by importnames
	;     defaults to all names, if not specified
	;   --select <parties> - choose voters with these party affiliations
	;     <parties> is comma separated list of parties ("*" to select all)
	;     only one of --select and --exclude are meaningful
	;   --stats <file> - optional output statistics of output voter list
	;   --voterid - output the voterid of each voter as the first column
	;   --zip - voters for selected zip codes, read from stdin, one per line
	;     --county and --zip are exclusive; default is --county
	; xref - rebuild cross reference index of voters
	; The optional [state] is state=XX where XX is the 2 letter abbreviation for
	; a state or territory (e.g., DC, which is not yet a state), defaulting to PA.

	; terminate on Ctrl-C if invoked from shell
	use $principal:(ctrap=$char(3):nocenable)
	; define error trap to print error message and terminate
	; and just terminate returning status to shell if an error is encountered
	; when executing the error trap
        set $etrap="set $etrap=""use $principal write $zstatus,! zhalt 1"""
        set $etrap=$etrap_" set tmp1=$piece($ecode,"","",2),tmp2=$text(@tmp1)"
        set $etrap=$etrap_" if $length(tmp2) write $text(+0),@$piece(tmp2,"";"",2,$length(tmp2,"";"")),!"
        set $etrap=$etrap_" do help zhalt +$extract(tmp1,2,$length(tmp1))"
	; raise error if top level entry is from anywhere except the shell
        set:$stack $ecode=",U254,"
	new addtrig,bornafter,bornbefore,cmdline,countylist,curate,cycles,deltrig,detailflag,fields,file
	new header,i,line,minmatch,names,op,patt,parties,selflag,sep,sort,state,statfile,stats,strength
	new tag,tmp,vidflag,vgender,ziplist
	; Initialization
	set $zdateform=2
	; Set defaults
	; - selflag=2 means exclude, 1 means select, 0 means select all ; default is --exclude "R"
	; - vidflag=1 means output voter id, 0 (default) means do not
	; - ziplist having data means selection is by zip codes
	set bornafter=0,bornbefore=$$FUNC^%DATE("TODAY")
	set (countylist,names)="*",parties=$$partypatt("R"),selflag="2",state="PA",vidflag=0,vgender=""
	; process command line; if no options specified, default is help
	set cmdline=$select($length($zcmdline):$zcmdline,1:"help")
	do:$$trimleadingstr^%XCMD(.cmdline,"state=")
	. set state=$zconvert($piece(cmdline," ",1),"u")
	. do trimleadingstr^%XCMD(.cmdline,state),trimleadingstr^%XCMD(.cmdline," ")
	for  quit:'$length(cmdline)  do
	. if $$trimleadingstr^%XCMD(.cmdline,"analyze") do
	. . set op="analyze",addtrig=.8,deltrig=.9,header=0,(fields,names)="",sep=$c(9)
	. . do trimleadingstr^%XCMD(.cmdline," ")
	. . for  quit:'$$trimleadingstr^%XCMD(.cmdline,"--")  do
	. . . if $$trimleadingstr^%XCMD(.cmdline,"addtrig") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set addtrig=+$zpiece(cmdline," ",1)
	. . . . set cmdline=$zpiece(cmdline," ",2,$zlength(cmdline," "))
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"curate") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set curate=+$zpiece(cmdline," ",1)\1
	. . . . set cmdline=$zpiece(cmdline," ",2,$zlength(cmdline," "))
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"deltrig") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set addtrig=+$zpiece(cmdline," ",1)
	. . . . set cmdline=$zpiece(cmdline," ",2,$zlength(cmdline," "))
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"fields") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set fields=$zpiece(cmdline," ",1)
	. . . . set cmdline=$zpiece(cmdline," ",2,$zlength(cmdline," "))
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"header") do
	. . . . set header=1
	. . . . set cmdline=$zpiece(cmdline," ",2,$zlength(cmdline," "))
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"names") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set tmp=$zpiece(cmdline," ",1)
	. . . . set names=$select(tmp?.E1N:tmp,1:$order(^names(tmp_$char(127)),-1))
	. . . . set cmdline=$piece(cmdline," ",2,$zlength(cmdline," "))
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"sep") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set sep=$zpiece(cmdline," ",1)
	. . . . set:""""=$zextract(sep,1) sep=$zwrite(sep,1)
	. . . . set cmdline=$zpiece(cmdline," ",2,$zlength(cmdline," "))
	. . . else  set $ecode=",U248,"
	. . set:'$zlength(names)!'$data(^names(names)) $ecode=",U242,"
	. . set:0'<+$get(curate,0) $ecode=",U243,"
	. . for i=1:1:$zlength(fields,",") set:1>$zpiece(fields,",",i) $ecode=",U240,"
	. . do analyzecurating quit
	. else  if $$trimleadingstr^%XCMD(.cmdline,"clean") do
	. . set:'$data(^fields(state)) $ecode=",U247,"
	. . do clean quit
	. else  if $$trimleadingstr^%XCMD(.cmdline,"expand") do
	. . set:'$data(^fields(state)) $ecode=",U247,"
	. . set op="expand",cycles=1,detailflag=0,minmatch=2,strength=.8
	. . do trimleadingstr^%XCMD(.cmdline," ")
	. . for  quit:'$$trimleadingstr^%XCMD(.cmdline,"--")  do
	. . . if $$trimleadingstr^%XCMD(.cmdline,"detail") set detailflag=1
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"cycles") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set cycles=+$piece(cmdline," ",1)\1
	. . . . set:1>cycles $ecode=",U248,"
	. . . . set cmdline=$piece(cmdline," ",2,$length(cmdline," "))
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"minmatch") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set minmatch=+$piece(cmdline," ",1)\1
	. . . . set:1>minmatch $ecode=",U248,"
	. . . . set cmdline=$piece(cmdline," ",2,$length(cmdline," "))
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"strength") do
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set strength=+$piece(cmdline," ",1)
	. . . . set:0>strength!(1<strength) $ecode=",U248,"
	. . . . set cmdline=$piece(cmdline," ",2,$length(cmdline," "))
	. . . else  set $ecode=",U248,"
	. . . do trimleadingstr^%XCMD(.cmdline," ")
	. . do expand quit
	. else  if $$trimleadingstr^%XCMD(.cmdline,"fields") do fields quit
	. else  if $$trimleadingstr^%XCMD(.cmdline,"help") do help quit
	. else  if $$trimleadingstr^%XCMD(.cmdline,"importnames") do
	. . set op="importnames"
	. . do trimleadingstr^%XCMD(.cmdline," ")
	. . set tag=$piece(cmdline," ",1)
	. . do trimleadingstr^%XCMD(.cmdline,tag),importnames(tag) quit
	. else  if $$trimleadingstr^%XCMD(.cmdline,"names") do
	. . set:'$data(^fields(state)) $ecode=",U247,"
	. . set op="names"
	. . do trimleadingstr^%XCMD(.cmdline," ") 
	. . for  quit:'$$trimleadingstr^%XCMD(.cmdline,"--")  do
	. . . if $$trimleadingstr^%XCMD(.cmdline,"analyze") do  quit
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set file=cmdline
	. . . . do trimleadingstr^%XCMD(.cmdline,file)
	. . . . do analyze(file)
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"find") do  quit
	. . . . do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set patt=cmdline
	. . . . do trimleadingstr^%XCMD(.cmdline,patt)
	. . . . do find(patt)
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"summarize") do summarizenames quit
	. . . else  set $ecode=",U248,"
	. . . do trimleadingstr^%XCMD(.cmdline," ")
	. else  if $$trimleadingstr^%XCMD(.cmdline,"importvoters") do
	. . set:'$data(^fields(state)) $ecode=",U247,"
	. . do importvoters quit
	. else  if $$trimleadingstr^%XCMD(.cmdline,"printvoters") do
	. . set:'$data(^fields(state)) $ecode=",U247,"
	. . set op="printvoters"
	. . do trimleadingstr^%XCMD(.cmdline," ")
	. . for  quit:'$$trimleadingstr^%XCMD(.cmdline,"--")  do
	. . . if $$trimleadingstr^%XCMD(.cmdline,"bornafter") do
	. . . . do trimleadingstr^%XCMD(.cmdline,"bornafter"),trimleadingstr^%XCMD(.cmdline," ")
	. . . . set bornafter=$piece(cmdline," ",1) do trimleadingstr^%XCMD(.cmdline,bornafter)
	. . . . set bornafter=$$FUNC^%DATE(bornafter)
	. . . . set:bornafter>bornbefore $ecode=",U244,"
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"bornbefore") do
	. . . . do trimleadingstr^%XCMD(.cmdline,"bornbefore"),trimleadingstr^%XCMD(.cmdline," ")
	. . . . set bornbefore=$piece(cmdline," ",1) do trimleadingstr^%XCMD(.cmdline,bornbefore)
	. . . . set bornbefore=$$FUNC^%DATE(bornbefore)
	. . . . set:bornafter>bornbefore $ecode=",U244,"
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"county") do
	. . . . do trimleadingstr^%XCMD(.cmdline,"county"),trimleadingstr^%XCMD(.cmdline," ")
	. . . . set countylist=$piece(cmdline," ",1) do trimleadingstr^%XCMD(.cmdline,countylist)
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"exclude") do
	. . . . do trimleadingstr^%XCMD(.cmdline,"exclude"),trimleadingstr^%XCMD(.cmdline," ")
	. . . . set parties=$piece(cmdline," ",1) do trimleadingstr^%XCMD(.cmdline,parties)
	. . . . set parties=$$partypatt(parties),selflag=2
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"gender") do
	. . . . do trimleadingstr^%XCMD(.cmdline,"gender"),trimleadingstr^%XCMD(.cmdline," ")
	. . . . set vgender=$piece(cmdline," ",1) do trimleadingstr^%XCMD(.cmdline,vgender)
	. . . . set vgender=$zwrite(vgender)
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"names") do
	. . . . do trimleadingstr^%XCMD(.cmdline,"names"),trimleadingstr^%XCMD(.cmdline," ")
	. . . . set names=$piece(cmdline," ",1) do trimleadingstr^%XCMD(.cmdline,names)
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"select") do
	. . . . do trimleadingstr^%XCMD(.cmdline,"select"),trimleadingstr^%XCMD(.cmdline," ")
	. . . . set parties=$piece(cmdline," ",1) do trimleadingstr^%XCMD(.cmdline,parties)
	. . . . if "*"=parties set selflag=0 kill parties
	. . . . else  set parties=$$partypatt(parties),selflag=1
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"stats") do
	. . . . set stats=1 do trimleadingstr^%XCMD(.cmdline," ")
	. . . . set statfile=$piece(cmdline," ",1) do trimleadingstr^%XCMD(.cmdline,statfile)
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"voterid") do
	. . . . do trimleadingstr^%XCMD(.cmdline,"select"),trimleadingstr^%XCMD(.cmdline," ")
	. . . . set vidflag=1
	. . . else  if $$trimleadingstr^%XCMD(.cmdline,"zip") do
	. . . . zsh "d" w "Kilroy was here",! for  read line quit:$zeof  do:""'=$translate(line," "_$char(9))
	. . . . . ; set:line'?1(5N,5N1"-"4N) $ecode=",U245,"
	. . . . . set ziplist(line)=""  zwr ziplist
	. . . . w "Kilroy was also here",! do trimleadingstr^%XCMD(.cmdline," ")
	. . . else  set $ecode=",U248,"
	. . . do trimleadingstr^%XCMD(.cmdline," ")
g	. . do printvoters($get(countylist,"*"),names) quit
	. else  if $$trimleadingstr^%XCMD(.cmdline,"xref") do
	. . set:'$data(^fields(state)) $ecode=",U247,"
	. . do xref quit
	. else  set $ecode=",U249,"
        . do trimleadingstr^%XCMD(.cmdline," ")
	quit

; Ad-hoc tagging for those instances where someone just provides a list of names
; and asks for them to be filtered to tag names belonging to one or more categories of names
; - namelist has several options
;   - if a single string, it is assumed to be a specific namelist, and it matches voters from that list
;   - if a list of comma separated strings, each piece is a namelist, and the output includes an
;     additional piece with the tags for each voter
;   Each namelist can be a specific list, e.g., Gujarati_20220901-1, or a general category, e.g.,
;   Gujarati, in which case getv will use the most recent list of that category
; - sep is a piece separator
; - pieces is a comma separated list of the pieces with names to be tagged;
;   blank names and initials to be omitted.
; - header - if numeric and non-zero, first line is a header; pass it through
; - omittag - is a namelist to not include in the tags, e.g, GeneralDesi, since it is a super
;   of other categories
;  Filters from stdin to stdout.
adhoc(namelist,sep,pieces,header,omittag)
	new flag1,flag2,i,j,line,names,nlists,npieces,piece,tmp
	set nlists=$zlength(namelist,",")
	for i=1:1:nlists do
	. set tmp=$zpiece(namelist,",",i)
	. set namelist(i)=$select(tmp?.E1N:tmp,1:$order(^names(tmp_$char(127)),-1))
	set npieces=$zlength(pieces,","),omittag=$get(omittag)
	if $get(header,0) read line write $zpiece(line,$char(13),1),$select(nlists-1:sep_"Tags",1:""),!
	for  read line quit:$zeof  set flag1=0,names="",line=$zpiece(line,$char(13),1) do
	. for j=1:1:nlists set flag2=0 do
	. . for i=1:1:npieces do  quit:flag2
	. . . set piece=$zconvert($zpiece(line,sep,$zpiece(pieces,",",i)),"u")
	. . . set:""""'=$zextract(piece,1) piece=$zwrite(piece)
	. . . if 1<$zlength(piece)&$data(^names(namelist(j),piece)) do
	. . . . set (flag1,flag2)=1,tmp=$zpiece(namelist(j),"_",1)
	. . . . set:tmp'=omittag names=names_$select($zlength(names):"+",1:"")_tmp
	. write:flag1 line,$select(nlists-1:sep_names,1:""),!
	quit

analyze(file)
	new fname,i,io,line,lname,mname,name,sep,x,y,xname
	kill names
	set io=$io
	set sep=$char(9)
	set:"UTF-8"'=$zchset $ecode=",U250,"
	set fname=^fields(state,"First Name")
	set lname=^fields(state,"Last Name")
	set mname=^fields(state,"Middle Name")
	open file:readonly use file
	for  read line quit:$zeof  do
	. for i=$length(line):-1:1 quit:" "'=$extract(line,i)
	. set:i'=$length(line) line=$extract(line,1,i)
	. do:$length(line) crunchname($zconvert($zwrite($select(""""=$extract(line,1):$piece(line,",",1),1:line)),"u"))
	use io close file
	do outputnames
	quit

analyzecurating
	; analyze results of curated set of names & output results
	new i,inlist,j,line,nnames,notinlist,tag,thisfield,thisname,tmp1,tmp2,voternames
	read:header line		; read and discard first line if column headers
	for i=header+1:1 read line quit:$zeof  do
	. set tag=$zconvert($ztranslate($zpiece(line,sep,curate)," "),"u")
	. do:$zlength(tag)&(tag'="U")&(tag'="-")	; a voter must be tagged Y or N to do analyze their names
	. . set:tag'?1(1"Y",1"N") $ecode=",U238,"
	. . set nnames=$zlength(names,",")
	. . for j=1:1:nnames do
	. . . set thisfield=$zpiece(fields,",",j),thisname=$zpiece(line,sep,thisfield)
	. . . if $zlength(thisname)-1,$increment(voternames(thisname,tag))
	set thisname=""
	for  set thisname=$order(voternames(thisname)) quit:'$zlength(thisname)  do
	. set tmp1=$get(voternames(thisname,"Y"),0),tmp2=$get(voternames(thisname,"N"),0)
	. set:tmp1!tmp2 voternames(thisname,"ratio")=tmp1/(tmp1+tmp2)
	. if $data(^names(names,$zwrite(thisname))) merge inlist(thisname)=voternames(thisname)
	. else  merge notinlist(thisname)=voternames(thisname)
	write "Candidates for removal",!
	set thisname=""
	for  set thisname=$order(inlist(thisname)) quit:'$zlength(thisname)  do:$data(voternames(thisname,"ratio"))
	. write:inlist(thisname,"ratio")<deltrig thisname,?16,$justify(inlist(thisname,"ratio"),0,2),?20,$justify($get(inlist(thisname,"Y"),0),6),?28,$justify(inlist(thisname,"N"),6),!
	write !,"Candates for addition",!
	set thisname=""
	for  set thisname=$order(notinlist(thisname)) quit:'$zlength(thisname)  do:1<$get(notinlist(thisname,"Y"),0)
	. write:notinlist(thisname,"ratio")>addtrig thisname,?16,$justify(notinlist(thisname,"ratio"),0,2),?20,$justify(notinlist(thisname,"Y"),6),?28,$justify($get(notinlist(thisname,"N"),0),6),!
	quit

clean	; clean voter records where first, middle or last name is non canonical
	; this routine must be run in UTF-8 mode
	set:"UTF-8"'=$zchset $ecode=",U250,"
	new fname,lname,mname,nextv,okpatt,prune,repl,sep,voterid,wspace,zprune
	set fname=^fields(state,"First Name")
	set lname=^fields(state,"Last Name")
	set mname=^fields(state,"Middle Name")
	set okpatt=".(1UN,1""Á"",1""À"",1""Ç"",1""È"",1""É"",1""Ë"",1""Í"",1""Ñ"",1""Ó"",1"" "",1""-"",1"""""""",1""'"",1""."",1""("",1"")"",1""/"",1""+"",1""`"",1""_"")"
	set prune=$char(8298,8217)_"$<=>,;:!\{}[]&?*`~#%"_$char(127,8216,8300,8928)	; characters to delete or replace
	set repl="U'S-"						; correct replacement characters
	set sep=$char(9)
	set wspace="1"""""""".(1"" "",1"""_$c(9)_""")1"""""""""
	set voterid=""
	set zprune=$zchar(201,209,220)
	view "nobadchar"
	for  set voterid=$order(^voters(state,voterid)) quit:""=voterid  do
	. set nextv=^voters(state,voterid)
	. set $piece(nextv,sep,fname)=$zconvert($piece($translate(nextv,prune,repl),sep,fname),"u")
	. set:$piece(nextv,sep,fname)?@wspace $piece(nextv,sep,fname)=""""""
	. set $piece(nextv,sep,mname)=$zconvert($piece($translate(nextv,prune,repl),sep,mname),"u")
	. set:$piece(nextv,sep,mname)?@wspace $piece(nextv,sep,mname)=""""""
	. set $piece(nextv,sep,lname)=$zconvert($piece($translate(nextv,prune,repl),sep,lname),"u")
	. set:$piece(nextv,sep,lname)?@wspace $piece(nextv,sep,lname)=""""""
	. set nextv=$ztranslate(nextv,zprune)
	. set:nextv'=^voters(state,voterid) ^voters(state,voterid)=nextv
	. ; The following are cases where the above did not clean the record and manual intervention is needed
	. do:$piece(nextv,sep,fname)'?@okpatt
	. . write "First name ",$zwrite($piece(nextv,sep,fname))," needs attention",! break
	. do:$piece(nextv,sep,mname)'?@okpatt
	. . write "Middle name ",$zwrite($piece(nextv,sep,mname))," needs attention",! break
	. do:$piece(nextv,sep,lname)'?@okpatt
	. . write "Last name ",$zwrite($piece(nextv,sep,lname))," needs attention",! break
	quit

crunchname(name)
	; accumulate statistics for name
	; internal label with common code, not to be called from outside routine
	; fname,lname,mname,sep expected to be set by caller
	new vfname,vid,vlname,vmname
	do:'$data(names(name))	; count occurrences of names
	. do:$data(^xref(state,"First Name",name))
	. . set vid="" for  set vid=$order(^xref(state,"First Name",name,vid)) quit:""=vid  do:$increment(names(name,"fname"))
	. . . set vlname=$piece(^voters(state,vid),sep,lname) if $increment(names(name,"fname",vlname,"lname"))
	. . . set vmname=$piece(^voters(state,vid),sep,mname) if $increment(names(name,"fname",vmname,"mname"))
	. do:$data(^xref(state,"Middle Name",name))
	. . set vid="" for  set vid=$order(^xref(state,"Middle Name",name,vid)) quit:""=vid  do:$increment(names(name,"mname"))
	. . . set vfname=$piece(^voters(state,vid),sep,fname) if $increment(names(name,"mname",vfname,"fname"))
	. . . set vlname=$piece(^voters(state,vid),sep,lname) if $increment(names(name,"mname",vlname,"lname"))
	. do:$data(^xref(state,"Last Name",name))
	. . set vid="" for  set vid=$order(^xref(state,"Last Name",name,vid)) quit:""=vid  do:$increment(names(name,"lname"))
	. . . set vfname=$piece(^voters(state,vid),sep,fname) if $increment(names(name,"lname",vfname,"fname"))
	. . . set vmname=$piece(^voters(state,vid),sep,mname) if $increment(names(name,"lname",vmname,"mname"))
	quit

expand	; read list of names from $principal and expand list of names with
	; associated names that meet a threshold
	; internal label, not intended to be called from outside routine
	; This routine can be made more efficient, but the initial focus is correctness.
	new doneflag,fname,i,lname,mname,name,names,namesin,newname,newnames,orignames,tmp,type,type1
	set sep=$char(9)			; field separator is tab
	set fname=^fields(state,"First Name")
	set lname=^fields(state,"Last Name")
	set mname=^fields(state,"Middle Name")
	for  read name quit:$zeof  set name=$zwrite(name),(namesin(name),orignames(name))=""
	set doneflag=0
	for i=1:1:cycles do  quit:doneflag
	. set name="" for  set name=$order(namesin(name)) quit:""=name  do crunchname(name)
	. set name="" for  set name=$order(names(name)) quit:""=name  for type="fname","lname","mname" do
	. . set newname="" for  set newname=$order(names(name,type,newname)) quit:""=newname  set:3<$length(newname)&'$data(namesin(newname)) newnames(newname)=""
	. kill names
	. set newname="" for  set newname=$order(newnames(newname)) quit:""=newname  do crunchname(newname)
	. kill newnames
	. set name="" for  set name=$order(names(name)) quit:""=name  for type="fname","lname","mname" do
	. . set newname="" for  set newname=$order(names(name,type,newname)) quit:""=newname  do:3<$length(newname)
	. . . for type1="fname","lname","mname" if $data(names(name,type,newname,type1)),$increment(newnames(name,$select($data(namesin(newname)):"match",1:"nomatch")),names(name,type,newname,type1))
	. kill names
	. set newname="" for  set newname=$order(newnames(newname)) quit:""=newname  do
	. . set tmp=$get(newnames(newname,"match"),0)
	. . if (tmp<minmatch)!(strength>(tmp/(tmp+$get(newnames(newname,"nomatch"))))) kill newnames(newname)
	. . else  do
	. . . write $zwrite(newname,1)
	. . . write:detailflag $char(9),newnames(newname,"match"),$char(9),$get(newnames(newname,"nomatch"),0)
	. . . write !
	. write "Cycles=",i,!
	. if $data(newnames) merge namesin=newnames kill newnames
	. else  set doneflag=1
	set name="" for  set name=$order(namesin(name)) quit:""=name  do:'$data(orignames(name))
	. write $zwrite(name,1)
	. write:detailflag $char(9),namesin(name,"match"),$char(9),$get(namesin(name,"nomatch"),0)
	. write !
	write "Cycles=",i,!
	quit

help    new j,k,label,tmp
        set label=$text(+0)
        for j=1:1 set tmp=$piece($text(@label+j),"; ",2) quit:""=tmp  do
        . write $piece(tmp,"$text(+0)",1) for k=2:1:$length(tmp,"$text(+0)") write $text(+0),$piece(tmp,"$text(+0)",k)
        . write !
        quit

fields	; read fields for each state. CAUTION: idiosyncratic and state specific
	if $data(^fields(state)) write "Deleting existing ^datefields(",state,"), ^fields(",state,")",!
	kill ^datefields(state),^fields(state)
	new file,field,i,io,line,piece,sep,tmp
	set io=$io
	set file=state_"Fields.txt"
	open file:readonly use file
	set sep=$char(9)	; default field separator is TAB, any state that differs must override
	if "GA"=state do
	. ; for GA metadata is first line of file; separator is vertical bar (|),
	. ; underscores are replaced by spaces, and converting to title case
	. ; Field Middle Maiden Name is copied to the canonical name "Middle Name"
	. set sep="|"
	. for i=1:1:1 read line
	. use io close file
	. for i=1:1:$length(line,sep) do
	. . set tmp=$piece(line,sep,i),piece=$zconvert($translate(tmp,"_"," "),"t")
	. . set:tmp?.E1"DATE".E ^datefields(state)=$select($data(^datefields(state)):^datefields(state)_","_piece,1:piece)
	. . set ^fields(state,piece)=i
	. ; add standard field names where printvoters needs it
	. set ^fields(state,"Middle Name")=^fields(state,"Middle Maiden Name")
	else  if "MI"=state do
	. ; for MI metata is the first line of voter data file with quoted field names
	. set sep=","
	. read line use io close file
	. for i=1:1:$zlength(line,sep) set ^fields(state,$zconvert($ztranslate($zwrite($zpiece(line,sep,i),1),"_"," "),"t"))=i
	. set ^datefields(state)="Year Of Birth,Registration Date"
	else  if "NC"=state do
	. ; for NC metadata is first line of file with text, and is made readable by deleting any trailing CR,
	. ; replacing underscores with spaces, and converting to title case
	. for  read line quit:""""=$extract(line,1)
	. use io close file
	. set line=$translate(line,"_"_$char(13)," ")
	. for i=1:1:$length(line,sep) set ^fields(state,$zconvert($zwrite($piece(line,sep,i),1),"t"))=i
	. set ^datefields(state)="Registr Dt,Birth Year"
	else  if "PA"=state do
	. for  read line quit:$zeof  do
	. . set piece=$piece(line,":",1)	; first piece in the line is the field
	. . set field=$piece(line,":",2)	; second piece is field name
	. . set ^fields(state,field)=piece
	. . set ^fields(state,field,"type")=$piece(line,":",3)
	. . if 3<$length(line,":") do
	. . . set (^fields(state,field,"comment"),tmp)=$piece(line,":",4)
	. . . set:"MM/DD/YYYY"=tmp ^datefields(state)=$select($data(^datefields(state)):^datefields(state)_","_piece,1:piece)
	. use io close file
	else  do
	. use io close file
	. set $ecode=",U247,"
	quit

find(patt)
	new namelist,namepos,state,thisname,tmp
	set patt=$zconvert(patt,"u")
	set state="" for  set state=$order(^xref(state)) quit:'$zlength(state)  do
	. for namepos="First Name","Middle Name","Last Name" do
	. . set thisname="" for  set thisname=$order(^xref(state,namepos,thisname)) quit:'$zlength(thisname)  do
	. . . set tmp=$zwrite(thisname,1)
	. . . set:tmp?@patt namelist(tmp)=""
	set thisname="" for  set thisname=$order(namelist(thisname)) quit:'$zlength(thisname)  write thisname,!
	quit

importnames(type)
	new line
	kill ^names(type)
	for  read line quit:$zeof  set:$length(line) ^names(type,$zwrite($zconvert($piece(line,",",1),"u")))=""
	quit

importvoters
	; read voters for current state from stdin. CAUTION: depends on state, idiosyncratic
	new datefields,err,errcnt,i,io,j,line,nflds,sep,todate,tmp1,tmp2,tmp3,zwrline
	set err="/proc/self/fd/2",errcnt=0
	set io=$io
	use $io:(chset="m")
	view "nobadchar"
	open err
	kill ^index(state),^voters(state)
	set datefields=^datefields(state)	
	set sep=$char(9)
	set todate=31+$zpiece($horolog,",",1)	; Allow dates 31 days into the future - see https://gitlab.com/ksbhaskar/getv/-/issues/27	
	if "GA"=state do	; GA doesn't quote data & uses "|" as a field separator; standardize on load
	. read line   		; discard first line as it is metadata
	. for i=1:1 read line quit:$zeof  do
	. . set zwrline=$zwrite($piece(line,"|",1))
	. . for j=2:1:$length(line,"|") set zwrline=zwrline_$char(9)_$zwrite($piece(line,"|",j))
	. . set ^voters(state,i)=zwrline
	else  do
	. if "NC"=state for i=1:1:2 read line	; discard first two lines for NC as they are a blank line & metadata
	. else  if "MI"=state read line		; discard first line for MI as it is metadata
	. for i=1:1 read line quit:$zeof  set line=$zsubstr(line,1) do
	. . do:"MI"=state			; MI has comma separated fields that contain commas
	. . . set nflds=$zlength(line,"""")
	. . . set tmp1=$zwrite($zpiece(line,"""",2))
	. . . for j=4:2:nflds do
	. . . . set tmp1=tmp1_$c(9)_$zwrite($zpiece(line,"""",j))
	. . . set line=tmp1
	. . for j=1:1:$zlength(datefields,",") do	; convert date fields to $horolog format
	. . . set tmp1=$zpiece(^datefields(state),",",j)
	. . . set:$char(0)']]tmp1 tmp1=^fields(state,$zpiece(datefields,",",j))
	. . . set tmp2=$ztranslate($zpiece(line,sep,tmp1),""""),tmp3=$select($char(0)']]tmp2:$$FUNC^%DATE(tmp2),1:tmp2)
	. . . do:'$zlength(tmp3)	; if tmp3 has a value, it has the date is already in $horolog format & no further conversion needed
	. . . . if 3=$zlength(tmp2,"-") set tmp3=$$FUNC^%DATE($zpiece(tmp2,"-",2)_"/"_$zpiece(tmp2,"-",3)_"/"_$zpiece(tmp2,"-",1))
	. . . . else  set:tmp2=+tmp2 tmp2="01/01/"_tmp2,tmp3=$$FUNC^%DATE(tmp2)		; date is just the  year
	. . . . if 0>tmp3!(tmp3>=todate) do	; date out of range - see https://gitlab.com/ksbhaskar/getv/-/issues/27
	. . . . . use err
	. . . . . write !,"Date out of range; field=",tmp1," Record follows",!,line
	. . . . . use io
	. . . . . set:$increment(errcnt) $zpiece(line,sep,tmp1)=""
	. . . . . set tmp3=""
	. . . set $zpiece(line,sep,tmp1)=tmp3
	. . set ^voters(state,i)=line
	write $fnumber(i,",")," voter records read. ",errcnt," date errors. Don't forget to clean & xref!",!
	close err
	quit

outputnames
	; output names generated by repeated calls to crunchname()
	; internal label with common code, not to be called from outside routine
	; fname,lname,mname,sep expected to be set by caller
	new name,x,xname,y
	write """List Name""",sep,"""Use""",sep,"""Assoc. With""",sep,"""First Name""",sep,"""Middle Name""",sep,"""Last Name""",!
	set name=""
	for  set name=$order(names(name)) quit:""=name  do
	. write name,sep,sep
	. for x="fname","mname","lname" write sep,$zwrite($get(names(name,x),0))
	. write !
	. for x="fname","mname","lname" do:$get(names(name,x),0)
	. . write sep,$zwrite($select("fname"=x:"First Name","mname"=x:"Middle Name",1:"Last Name")),!
	. . set xname=""
	. . for  set xname=$order(names(name,x,xname)) quit:""=xname  do
	. . . write sep,sep,$select(""""""=xname:"<none>",1:xname)
	. . . for y="fname","mname","lname" write sep,$zwrite($get(names(name,x,xname,y)))
	. . . write !
	quit

partypatt(partylist)
	; Convert partylist into regular expresion for matching
	new i,n,patt
	set n=$length(partylist,",")
	if 1=n set patt="1"""""""_partylist_""""""""
	else  do
	. set patt="1"""_$piece(partylist,",",1)_""""
	. for i=2:1:n set patt=patt_",1"""_$piece(partylist,",",i)_""""
	. set patt="1""""""""1("_patt_")1"""""""""
	quit patt

printvoters(countylist,selname)
	; Print voters for selected countylist and name list
	; CAUTION: idiosyncratic
	; TODO: Generalize to states other than PA
	use $principal:(ctrap=$char(3):nocenable:exception="halt")         ; terminate on Ctrl-C if invoked from shell
	new county,datefields,deceased,dflag,dob,flist,fname,fpiece,gender,i,j,io,line,lname,mname,nextdob,nextsel,nextfname,nextgender,nextlname,nextmname,nextpcode,nextv,nextvid,nextvoter,nfields,pcode,reason,sel,sep,sthouse,stsenate,thissel,tmp1,tmp2,ushouse,vidsub,voterreg,zip
	set io=$io
	set (dflag,deceased)=0			; deceased flag only used when states report it
	set datefields=^datefields(state)
	set outfields=state_"outputfields.txt"	; file selecting output fields
	set sep=$char(9)			; field separator is tab
	set fname=^fields(state,"First Name")
	set lname=^fields(state,"Last Name")
	set mname=^fields(state,"Middle Name")
	set vidsub=9				; which ^index() subscript has the voter id
	if "GA"=state do
	. set county=^fields(state,"County Code")
	. set dob=^fields(state,"Birthdate")
	. set gender=^fields(state,"Gender")
	. set pcode=^fields(state,"Party Last Voted")
	. set sthouse=^fields(state,"House District")
	. set stsenate=^fields(state,"Senate District")
	. set ushouse=^fields(state,"Congressional District")
	. set voterreg=^fields(state,"Registration Number")
	. set zip=^fields(state,"Residence Zipcode")
	else  if "MI"=state do
	. set county=^fields(state,"County Name")
	. set dob=^fields(state,"Year Of Birth")
	. set gender=^fields(state,"Gender")
	. set pcode=""
	. set sthouse=^fields(state,"State House District Code")
	. set stsenate=^fields(state,"State Senate District Code")
	. set ushouse=^fields(state,"Us Congress District Code")
	. set voterreg=^fields(state,"Voter Identification Number")
	. set zip=^fields(state,"Zip Code")
	else  if "NC"=state do
	. set county=^fields(state,"County Desc")
	. set dflag=1				; NC voter rolls report deceased voters
	. set dob=^fields(state,"Birth Year")
	. set gender=^fields(state,"Gender Code")
	. set pcode=^fields(state,"Party Cd")
	. set reason=^fields(state,"Voter Status Reason Desc")
	. set voterreg=^fields(state,"Voter Reg Num")
	. set zip=^fields(state,"Zip Code")
	else  if "PA"=state do
	. set county=^fields(state,"County")
	. set dob=^fields(state,"DOB")
	. set gender=^fields(state,"Gender")
	. set pcode=^fields(state,"Party Code")
	. set sthouse=^fields(state,"State House")
	. set stsenate=^fields(state,"State Senate")
	. set ushouse=^fields(state,"US House")
	. set voterreg=^fields(state,"ID Number")
	. set zip=^fields(state,"Zip")
	else  set $ecode=",U247,"	; must check for illegal state because this could be first entry in routine
	; Get list of desired output fields
	open outfields:readonly use outfields
	for nfields=1:1 read line quit:$zeof  set flist(nfields)=line,fpiece(nfields)=^fields(state,line)
	use io
	if $increment(nfields,-1)		; number of output fields is one less because last value was EOF
	; write output file header
	do:vidflag
	. if "GA"=state do
	. . write $zwrite("Registration Number"),sep
	. else  if "MI"=state do
	. . write $zwrite("Voter Id"),sep
	. else  if "NC"=state do
	. . write $zwrite("County Desc"),sep,$zwrite("Voter Reg Num"),sep
	. else  if "PA"=state do
	. . write $zwrite("Voter Id"),sep
	write $zwrite(flist(1))
	for i=2:1:nfields write sep,$zwrite(flist(i))
	write !
	; expand/replace countylist with array of alphabetized county names as subscripts
	do:'$data(ziplist)	; if ziplist has data, process selected zips, not selected counties
	. if "*"=countylist set i="" for  set i=$order(^index(state,i)) quit:""=i  set countylist(i)=""
	. else  for i=1:1:$length(countylist,",") set countylist($zwrite($piece(countylist,",",i)))=""
	. zkill countylist
	; write selected fields of selected voters with selected names, ordered by county
	if $data(ziplist) set *sel=ziplist
	else  set *sel=countylist
	set thissel="" for  set thissel=$order(sel(thissel)) quit:""=$zwrite(thissel,1)  do
	. set nextv=$query(^index(state,thissel,""))
	. for  set nextsel=$qsubscript(nextv,2) quit:nextsel'=thissel  do  set nextv=$query(@nextv) quit:""=nextv
	. . set nextvid=$qsubscript(nextv,vidsub),nextvoter=^voters(state,nextvid)
	. . set nextdob=$piece(nextvoter,sep,dob)
	. . set nextgender=$piece(nextvoter,sep,gender)
	. . set nextfname=$piece(nextvoter,sep,fname)
	. . set nextlname=$piece(nextvoter,sep,lname)
	. . set nextmname=$piece(nextvoter,sep,mname)
	. . set nextpcode=$piece(nextvoter,sep,pcode)
	. . set:dflag deceased="""DECEASED"""=$piece(nextvoter,sep,reason)
	. . do:(("*"=selname)!($data(^names(selname,nextfname))!$data(^names(selname,nextmname))!$data(^names(selname,nextlname))))&'deceased
	. . . do:$select(2=selflag:nextpcode'?@parties,1=selflag:nextpcode?@parties,1:1)&(nextdob>=bornafter)&(nextdob<=bornbefore)&('$zlength(vgender)!(nextgender=vgender))
	. . . . do:vidflag
	. . . . . if "GA"=state do
	. . . . . . write $piece(nextvoter,sep,voterreg),sep
	. . . . . else  if "MI"=state do
	. . . . . . write $piece(nextvoter,sep,voterreg),sep
	. . . . . else  if "NC"=state do
	. . . . . . write $piece(nextvoter,sep,county),sep,$piece(nextvoter,sep,voterreg),sep
	. . . . . else  if "PA"=state do
	. . . . . . write $piece(nextvoter,sep,voterreg),sep
	. . . . write $piece(nextvoter,sep,fpiece(1))
	. . . . for j=1:1:$zlength(datefields,",") do
	. . . . . set tmp1=$zpiece(datefields,",",j),tmp2=$zpiece(nextvoter,sep,tmp1)
	. . . . . set:$zlength(tmp2) $zpiece(nextvoter,sep,tmp1)=$zdate($select(0<tmp2:tmp2,1:0))
	. . . . for j=2:1:nfields write sep,$piece(nextvoter,sep,fpiece(j))
	. . . . write !
	. . . . ; Accumulate statistics if requested. Overly long line for efficiency as this code is innermost code in a loop
	. . . . if $data(stats)&$increment(stats("Zip",$piece(nextvoter,sep,zip)))&$increment(stats("County",$piece(nextvoter,sep,county)))&$increment(stats("Names",selname))&$increment(stats("Names",selname,nextfname,"fname"))&$increment(stats("Names",selname,nextmname,"mname"))&$increment(stats("Names",selname,nextlname,"lname"))
	; Output statistics if requested
	do:$data(stats)
	. open statfile:newversion use statfile
	. write "Tagged",sep,stats("Names",selname),!
	. for i="County","Zip" do
	. . write "By ",i,!
	. . set j="" for  set j=$order(stats(i,j)) quit:""=j  write sep,j,sep,stats(i,j),!
	. write "By Name",!,sep,"Name",sep,"First",sep,"Middle",sep,"Last",!
	. set i="" for  set i=$order(stats("Names",selname,i)) quit:""=i  do
	. . write sep,i,sep,$get(stats("Names",selname,i,"fname")),sep,$get(stats("Names",selname,i,"mname")),sep,$get(stats("Names",selname,i,"lname")),!
	. use io close statfile
	quit

procnames(file)
	new i,io,line,name,sep,tmp,x
	kill names
	set io=$io
	set sep=$char(9)
	open file:readonly use file
	for  read line quit:$zeof  do:$length(line)
	. set name=$zconvert($zwrite($select(""""=$extract(line,1):$piece(line,",",1),1:$piece(line," ",1))),"u")
	. ; avoid overcounting in case name appears more than once, but report count of each name in input
	. if $increment(names(name,0)) kill names(name,1),names(name,2),names(name,3)
	. set x="" for  set x=$order(^xref(state,"First Name",name,x)) quit:""=x  if $increment(names(name,1))
	. set x="" for  set x=$order(^xref(state,"Middle Name",name,x)) quit:""=x  if $increment(names(name,2))
	. set x="" for  set x=$order(^xref(state,"Last Name",name,x)) quit:""=x  if $increment(names(name,3))
	use io close file
	write """Name""",sep,"""On List""",sep,"""#First""",sep,"""#Middle""",sep,"""#Last""",!
	set name=""
	for  set name=$order(names(name)) quit:""=name  do
	. write name,sep,$zwrite(names(name,0))
	. for i=1:1:3 write sep,$zwrite($get(names(name,i)))
	. write !
	quit

summarizenames
	new fname,i,io,lname,mname,name,sep,vname
	kill names
	set io=$io
	set sep=$char(9)
	set:"UTF-8"'=$zchset $ecode=",U250,"
	set fname=^fields(state,"First Name")
	set lname=^fields(state,"Last Name")
	set mname=^fields(state,"Middle Name")
	for name="First Name","Middle Name","Last Name" do
	. set vname=""
	. for  set vname=$order(^xref(state,name,vname)) quit:""=vname  do crunchname(vname)
	do outputnames
	quit

xref	; cross reference voters by County, Date of Birth,Precinct Code, Zip, Street Name, House Number, Last Name, First Name
	; CAUTION: idiosyncratic; depends on state
	; TODO: Move sthouse, stsenate, ushouse to common area once extended from PA to other states
	new county,fname,housenum,lname,mname,nextv,precinct,resaddr,sep,sthouse,stname,stnamsuffix,stsenate,tmp,ushouse,votrereg,zip,zprune
	kill ^index(state),^xref(state)
	set sep=$char(9)
	set fname=^fields(state,"First Name")
	set lname=^fields(state,"Last Name")
	set mname=^fields(state,"Middle Name")
	view "nobadchar"
	if "GA"=state do
	. set county=^fields(state,"County Code")
	. set housenum=^fields(state,"Residence House Number")
	. set precinct=^fields(state,"County Precinct Id")
	. set stname=^fields(state,"Residence Street Name")
	. set stnamesuffix=^fields(state,"Residence Street Suffix")
	. set zip=^fields(state,"Residence Zipcode")
	else  if "MI"=state do
	. set county=^fields(state,"County Name")
	. set dob=^fields(state,"Year Of Birth")
	. set housenum=^fields(state,"Street Number")
	. set precinct=^fields(state,"Precinct")
	. set sthouse=^fields(state,"State House District Code")
	. set stname=^fields(state,"Street Name")
	. set stsenate=^fields(state,"State Senate District Code")
	. set ushouse=^fields(state,"Us Congress District Code")
	. set zip=^fields(state,"Zip Code")
	else  if "NC"=state do
	. set county=^fields(state,"County Desc")
	. set precinct=^fields(state,"Precinct Abbrv")
	. set resaddr=^fields(state,"Res Street Address")
	. set voterreg=^fields(state,"Voter Reg Num")
	. set zip=^fields(state,"Zip Code")
	else  if "PA"=state do
	. set county=^fields(state,"County")
	. set dob=^fields(state,"DOB")
	. set housenum=^fields(state,"House Number")
	. set precinct=^fields(state,"Precinct Code")
	. set sthouse=^fields(state,"State House")
	. set stname=^fields(state,"Street Name")
	. set stsenate=^fields(state,"State Senate")
	. set ushouse=^fields(state,"US House")
	. set zip=^fields(state,"Zip")
	else  set $ecode=",U247,"
	set nextv=""
	for  set nextv=$order(^voters(state,nextv)) quit:""=nextv  do
	. set tmp=$zsubstr(^voters(state,nextv),1)
	. if "GA"=state do ; since GA data is not quoted, need 
	. . set ^index(state,$piece(tmp,sep,county),$piece(tmp,sep,precinct),$piece(tmp,sep,zip),$piece(tmp,sep,stname)_$select($length($piece(tmp,sep,stnamesuffix)):" "_$piece(tmp,sep,stnamesuffix),1:""),$piece(tmp,sep,housenum),$piece(tmp,sep,lname),$piece(tmp,sep,fname),nextv)=""
	. else  if "NC"=state do
	. . set ^index(state,$piece(tmp,sep,county),$piece(tmp,sep,precinct),$piece(tmp,sep,zip),$piece(tmp,sep,resaddr),$piece(tmp,sep,lname),$piece(tmp,sep,fname),$piece(tmp,sep,voterreg),nextv)=""
	. else  if ("MI"=state)!("PA"=state) do
	. . set ^index(state,$piece(tmp,sep,county),$piece(tmp,sep,precinct),$piece(tmp,sep,zip),$piece(tmp,sep,stname),$piece(tmp,sep,housenum),$piece(tmp,sep,lname),$piece(tmp,sep,fname),nextv)=""
	. . set ^xref(state,"DOB",+$piece(tmp,sep,dob),nextv)=""
	. . set ^xref(state,"State House",$piece(tmp,sep,sthouse),nextv)=""
	. . set ^xref(state,"State Senate",$piece(tmp,sep,stsenate),nextv)=""
	. . set ^xref(state,"US House",$piece(tmp,sep,ushouse),nextv)=""
	. set ^xref(state,"First Name",$piece(tmp,sep,fname),nextv)=""
	. set ^xref(state,"Last Name",$piece(tmp,sep,lname),nextv)=""
	. set ^xref(state,"Middle Name",$piece(tmp,sep,mname),nextv)=""
	. set ^xref(state,"Zip",$piece(tmp,sep,zip),nextv)=""
	quit

;       Error message texts
U238	;"-F-INVTAG Tag "_tag_" in line "_i_"is not Y, N, U, or blank"
U239	;"-F-INVCURATE Line "_i_" curating field "_curate_" has invalid value "_cvalue_" Line is: "_line
U240	;"-F-INVFIELDS Not all fields specified: """_fields_""" are integers greater than 0"
U241	;"-F-INVSORT Sort type "_sort_" is outside the range -2 through 2"
U242	;"-F-INVNAMES Name list """_names_""" does not exist"
U243	;"-F-NOCURATE Piece with curating info is not a positive integer: "_$get(curate)
U244	;"-F-INVDOB bornafter="_$zdate(bornafter)_" is after bornbefore="_$zdate(bornbefore)
U245	;"-F-INVZIP "_line_" is not a valid zip code"
U246	;"-F-FIELDSEXIST ^fields("_state_") already has data"
U247	;"-F-UNKSTATE Unknown state or territory "_state
U248	;"-F-ILLOPTION Illegal command line for operation "_op_" starting with --"_cmdline
U249    ;"-F-ILLCMDLINE Illegal command line starting with: --"_cmdline
U250	;"-F-NEEDUTF8MODE clean needs to be done in UTF-8 mode"
U254    ;"-F-LABREQ Invocation from another program must specify a label;  use yottadb -run "_$text(+0)_" to execute from top of routine