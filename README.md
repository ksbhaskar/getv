# getv - Get Out the Vote

Democracies only work well when voters vote in elections. This software is designed to help public-spirited individuals and organizations create get-out-the-vote campaigns from voter rolls, which, in the United States are available from State Governments. For example, the voter rolls for the State of Pennsylvania can be [obtained from the Department of State](https://www.pavoterservices.pa.gov/Pages/PurchasePAFULLVoterExport.aspx).

# Requirements

This software is written to run on [YottaDB](https://yottadb.com) or [GT.M](https://sourceforge.net/projects/fis-gtm) on a supported distribution of Linux, such as [Ubuntu](https://ubuntu.com). More specifically, it has been tested on Ubuntu 18.04 LTS running YottaDB r1.22 with data for voters in [Chester County, Pennsylvania](http://www.chesco.org/).

While the examples are all from Chester County in the State of Pennsylvania, the software should be usable by other states as well.

# Nota Bene

This software is still very much under development. While it is quite functional and very usable in its current state, this means:

- It is inefficient. By using indexes, it should be able to return results at least two orders of magnitude faster.
- It requires someone comfortable working in the Linux command line. If you are not comfortable working in the Linux command line, seek assistance from someone who is.
- It would benefit from better ways to include or avoid voters. For example, while is is possible to tell the program to include anyone with the last name "BHASKAR" (Pennsylvania voter names are all in upper case), one might want to include everyone whose last name starts with "BHA".
- It is not very tolerant of mistakes. To err is human, and when you make a mistake, it should give you a helpful error message. At present, it is expert-friendly rather than user-friendly.

I intend to improve functionality as well as performance. As my expertise is in programs that run from the Linux shell command line, I invite collaborators who would like to webify the user interface, and to create user documentation.

To report bugs or misfeatures, or to suggest improvements, please [create an Issue](/../issues). Better yet, if you are a developer, please fork the project, make your changes and send me a Merge Request.

# Theory of Operation

Voter rolls are just tabular data.[^1] In the case of Pennsylvania voter rolls, the first column is the voter id, and subsequent columns (the last is number 153) are data for that voter. Each column has a label (in the case of Pennsylvania, provided in a separate file). Some columns will have actual data (such as Last Name), other columns will have representations of the data, e.g, "D" for party affiliation with the Democratic Party.

[^1]: It is reasonable to ask why I did not use an off-the-shelf relational datebase management system. First, programming YottaDB in M is what I know best and I want to follow the "release early, release often" mantra of free / open source software. Second, although the data are tabular, they would need to be broken into multiple tables for normalization.

## Schema

- Data about each voter is stored in the global variable `^voters` whose sole subscript is the voter id. Tab separated pieces in the value of that node are the data for that voter.
- Fields definitions are stored in the global variable `^fields`. The sole subscript is the field name (e.g., `"Address Line 2"`), and the node value is the column number, e.g. `17` - so in this example, `^fields("Address Line 2")=17`
- The global variable `^index` holds cross reference indexes for to accelerate access to data. Cross references are built on demand when needed, and all cross references are deleted when voter data is loaded. At present, cross references are used only for limited use cases of access to the data, and application speedup will be implemented by using cross references for additional use cases.
- The raw data is in the global variable `^voters`. The subscript of each node is the voter ID Number (assumed to be the first field column of the data provided by the state), and the value at each node is a string consisting of tab separated columns 2 through the last column.

# Instructions for use

In the following, the terms "column" and "field" are used interchangeably.

## Get the voter rolls from the State of Pennsylvania for the county or counties you want

At the [PA Full Voter Export](https://www.pavoterservices.pa.gov/pages/purchasepafullvoterexport.aspx) site of the Pennsylvania Department of State, you can request voter rolls for non-commercial purposes. The current fee is $20. You will get a link to download a zip file (e.g., `CHESTER.ZIP` for Chester county). Unzip the file in a directory, and you will see a set of files like this:

```
$ ls -l
total 218672
-rw-r--r-- 1 user users      1996 Mar 28 05:00 'CHESTER Election Map 20180328.txt'
-rw-r--r-- 1 user users 223850688 Mar 28 05:16 'CHESTER FVE 20180328.txt' 
-rw-r--r-- 1 user users     24652 Mar 28 05:00 'CHESTER Zone Codes 20180328.txt'        
-rw-r--r-- 1 user users       424 Mar 28 05:00 'CHESTER Zone Types 20180328.txt'
$ 
```

The file of interest is the large file, in this case `CHESTER FVE 20180328.txt`, which has the raw table of data from the State. You will also get a file `Voter.docx` that contains a table with a description of the fields, i.e., that Column 1 is `"ID Number"`, column 2 is `"Title"`, columnn 3 is `"Last Name"`, and so on (the case and spaces are important), as well as the type of data that field contains (usually `"String"`) and relevant notes, e.g., for column 7 (`"Gender"`) that the values are `"F=Female, M=Male, U=Unknown"`.

## Create file with metadata describing fields

Create a text file, each of whose lines corresponds to a column. Each line consists of three colon (`:`) separated pieces:

1. Column number, e.g., `10`
1. Field Name, e.g. `Vote Status`
1. Data type, e.g., `String`
1. Optional comments, e.g., `A=Active, I=Inactive`

Of these, only the first two are important to the operation of [getv](https://gitlab.com/ksbhaskar/getv).

The file [PAFields.txt](PAFields.txt) is the file I created from the Chester County voter records I acquired from the State of Pennsylvania. You may find it easier to start with that file and edit it for changes between the file I acquired and the file you acquire.

## Install YottaDB on your Linux system

The software is written to run on [YottaDB](https://yottadb.com). It should also run on GT.M (https://sourceforge.net/projects/fis-gtm), but has not been tested with the latter.

Follow the instructions on the [YottaDB Getting Started page](https://yottadb.com/product/get-started/) to install YottaDB on your system. YottaDB is tested on Ubuntu and Red Hat, but should work on contemporary releases of popular Linux distributions. If in doubt, or if you are running another computing platform such as Windows or OS X, use a Docker container with a pre-built image following instructions on that page.

Create a directory where YottaDB is to work and set the environment variable `ydb_dir` to point to it (YottaDB will create the directory structure if one does not exist; and use an existing one if it does). If you do not choose otherwise, the default is `$HOME/.yottadb`. Execute the command `source <ydb_installed_dir>/ydb_env_set` to create the environment. The default installation of YottaDB is `/usr/local/lib/yottadb/r122` where `r122` is the YottaDB release, r1.22 in this example. When you install and use YottaDB for getv, it may be a newer release, e.g., `.../r124/`.

---
**Note**: The operation of YottaBD is configurated and controlled by environment variables. Every time you want to run YottaDB in a fresh shell, execute: `export ydb_dir=<xxx>` where `xxx` is the directory where you want YottaDB to work, unless you are using the default `$HOME/.yottadb`.

---

## Install getv on your system

Download [getv]() and unpack the distribution, or get the individual files.

Place the file [getv.m](getv.m) in the `r/` subdirectory of your work directory. For example if you are using the default location of `$HOME/.yottadb`, then place it in `$HOME/.yottadb/r`; otherwise, place it in `$ydb_dir/r/`.

Place the file [getv.sh](getv.sh) in a location where you normally place executable files (e.g., `$HOME/bin`). Edit it to change the line `export ydb_dir=...` to set the environment variable to your working directory for getv, and the directory `/usr/local/lib/yottadb/r122` to the directory where YottaDB is installed.

## Run getv.sh

To run getv, you can either:

- Run the `getv.sh [options]` command, or
- source the `ydb_env_set` file, e.g., `source /usr/local/lib/yottadb/r122` for YottaDB r1.22 and then `mumps -run getv [options]`.

The `--help` option gets you a full list of options, e.g.

```bash
$ ./getv.sh --help
Interact with Pennsylvania voter data provided by the Sate
Usage: mumps -run getv [options] where options are zero or more of:
--avoidany filename
  Do not includeany any voters who meet at least one criterion of those specified in filename. The file
  consists of lines of three colon (:) separated items:
  1. A field name loaded by --loadfields option.
  2. A MUMPS relational operator (https://docs.yottadb.com/ProgrammersGuide/langfeat.html#operators)
  3. A value (string or number depending on the operator
  If an avoid file is is not specified, no voters are avoided.
--dumpfields - on stdout dump current field names in a format that --loadfields can load
--dumpvoters - on stdout dump current voter data in a format that --loadvoters can load
--help - print information on using the program
--includeanyany filename
  Includeany only those voters who meet at least one criterion of those specified in filename. The file
  consists of lines of three colon (:) separated items:
  1. A field name loaded by --loadfields option.
  2. A MUMPS relational operator (https://docs.yottadb.com/ProgrammersGuide/langfeat.html#operators)
  3. A value (string or number depending on the operator
  If an includeany file is not specified, all voters are includeanyd.
--loadfields - from stdin load a file whose lines are colon separated fields as follows:
  columnnumber - the column number of an item in a data file
  fieldname - the field name of an item in a data file column
  type - the data type of the item
  comment - any comments pertaining to that item
  The loadfields options kills and replaces the global variable ^fields.
  Column names are optional - columns can be accessed by column number.                                                                     
--loadvoters - from stdin load a file whose lines are tab separated fields with each value in double quotes:                                
  The first field is the SURE voted id, and the remaining fields are as described by the file                                               
  loaded with the --loadfields command.
  The loadvoters option kills and replaces the global variable ^voters, and kills ^index.
--output filename to write on stdout a list of fields for each voter selected, where fields are read from filename
  one to a line. The output is tab separated and ready to load into a spreadsheet program of your choice.
--summarize fieldname
  For the specified fieldname, print a summary of values and counts.
  Voters listed are those who meet at least one inclide criterion and no avoid criteria. Multiple --avoidany and
  --includeany options on a line can be used to apply criteria from multiple sources.
  Note that some operations (dumpfields, dumpvoters, help, and summarize) do their work and end the program.
  Therefore, options that affect the operations do their work and end the program (such as --avoidany and
  --includeany for --summarize) should precede the latter.
$ 
```
See the [Sample Project](#sample-project) for examples of use.

## Load the metadata with field definitions

Use the `--loadfields` option to load the field definitions, e.g.,

```bash
$ ./getv.sh --loadfields <PAFields.txt
154 field descriptions read
$ 
```

## Load the voter data

Use the `--loadvoters` option to load the voters, e.g.,

```bash
$ ./getv.sh --loadvoters <CHESTER\ FVE\ 20180328.txt 
347,317 voter records read
$
```

## Test the load

For example, to identify the political groups recognized in Pennsylvania (the output is a tab-separated text file suitable for loading into a spreadsheet for further analysis):

```bash
$ ./getv.sh --summarize Party Code
Unique values   ""      95
""      ""      481
""      "3RD"   2
""      "AC"    2
""      "AD"    5
...
""      "D"     134993
...
""      "R"     150982
...
$
```

This means there are 95 unique political groups recognized by the Pennsylvania. Not all of them are political parties - for example, I refers to INDEPENDENT and OTH refers to OTHER. There are also parties that I had not heard of before I started this project, such as JD for JEDI or MYMED for MEN YOGA MEDITATION. One learns something every day...


# Sample Project

The use of getv is illustrated with a sample project. Let's say you want to create a phone chain to get South Asian-Americans to remind them to vote. You might try to identify South Asians by their names:[^2]

[^2]: This is not a guarantee of success because there it would miss the South Asian [George Fernandes](https://en.wikipedia.org/wiki/George_Fernandes), were he a voter, and would include the non-South Asian [Vidiadhar Surajprasad Naipaul](https://en.wikipedia.org/wiki/V._S._Naipaul), again were he a voter. But we are aiming for a technique that is good enough, not one that is perfect.

You might try summarizing last names:

```bash
$ ./getv.sh --summarize Last Name >UniqueLastName.txt
$ head -1 UniqueLastName.txt 
Unique values   ""      61657
$ 
```
It's probably a hopeless task to manually go through 61,657 last names to identify the South Asian names. Suppose you decide to skip those who are registered Republican and those living in Nottingham because they already have effective phone chains. You create a file with colon separated fields of those we want to avoid, and then run the program again:

```bash
$ cat avoid.txt
Party Code:=:"R"
City:=:"NOTTINGHAM"
$ ./getv.sh --avoidany avoid.txt --summarize Last Name >UniqueLastName1.txt
$ head -1 UniqueLastName1.txt 
Unique values   ""      48448
$  
```
48,448 names is still too many to go through and select manually. So another approach is warranted.

You can try building up a list from scratch. Since Kumar is a reasonably common South Asian name, you might create a file to include anyone whose first, middle, or last name is Kumar, and get unique first and last names from that summary:

```bash
$ cat include.txt
First Name:=:"KUMAR"
Middle Name:=:"KUMAR"
Last Name:=:"KUMAR"
$ ./getv.sh --avoidany avoid.txt --includeany include.txt --summarize Last Name >UniqueLastName2.txt
$ head -1 UniqueLastName2.txt 
Unique values   ""      52
$ ./getv.sh --avoidany avoid.txt --includeany include.txt --summarize Middle Name >UniqueMiddleName2.txt
$ head -1 UniqueMiddleName2.txt 
Unique values   ""      6
$ ./getv.sh --avoidany avoid.txt --includeany include.txt --summarize First Name >UniqueFirstName2.txt
$ head -1 UniqueFirstName2.txt 
Unique values   ""      64
$ 
```

This now becomes a more tractable problem. You can go through the files of unique first, middle and last names and add to `include.txt` those first, middle, and last names. Then you can run the command again to get more first, middle, last names and add those to the include list. As you review the names, perhaps you will decide to add some to the avoid list. Once you have include and avoid lists with which you are satisfied, create a file with the output fields you need to create your phone chain. Then you can run it to generate a list of names that you can load into your spreadsheet to develop your phone chain. For example:

```bash
$ cat outputspec1.txt 
First Name
Middle Name
Last Name
Gender
DOB
House Number
House Number Suffix
Street Name
Apartment Number
Address Line 2
City
State
Zip
Home Phone
County
Precinct Code
Precinct Split ID
$ ./getv.sh --avoidany avoid.txt --includeany include.txt --output outputspec1.txt >outputVoters.txt
$ wc outputVoters.txt 
  2270  42354 291138 outputVoters.txt
$ 
```

So now you have a list of 2,269 voters (the first line is line of column names) that you can import into a spreadsheed and for whom you can create your phone chain.

# Footnotes